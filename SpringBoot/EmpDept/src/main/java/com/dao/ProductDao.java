package com.dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class ProductDao {

	@Autowired
	ProductRepository productRepository;
	
	public List<Product> getAllProducts() {
		return productRepository.findAll();
		
	}

	public Product getProductById(int prodId) {
		return productRepository.findById(prodId).orElse(null);
	}

	public Product getProductByName(String prodName) {
		return productRepository.findByName(prodName);
	}

	public Product addProduct(Product product) {
		return productRepository.save(product);
	}

	public Product updateProduct(Product product) {
		return productRepository.save(product);
	}

	public void deleteProductById(int prodId) {
		productRepository.deleteById(prodId);

	}

}
