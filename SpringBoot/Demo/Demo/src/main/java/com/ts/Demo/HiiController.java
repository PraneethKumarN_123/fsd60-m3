package com.ts.Demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HiiController {
	
	@RequestMapping("helloMethod")
	public String helloMethod(){
		return "hello method from hellocontroller";
	}
	
	
}