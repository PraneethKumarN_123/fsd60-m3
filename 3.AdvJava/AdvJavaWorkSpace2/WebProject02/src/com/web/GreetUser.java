package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/GreetUser")
public class GreetUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

  
    public GreetUser() {
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String userName = request.getParameter("userName");
		PrintWriter out = response.getWriter();
		out.print("<html>");
		out.print("<body bgcolor='lightblue' text='yellow'>");
		out.print("<h1> Welcome "+ userName + "!</h1>");
		out.print("</body>");
		out.print("</html>");




		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
