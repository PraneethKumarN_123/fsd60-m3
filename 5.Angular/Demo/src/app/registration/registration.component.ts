import { Component,OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrl: './registration.component.css'
})
// export class RegistrationComponent implements OnInit {

//     employee : any;
//     countries: any;


//     constructor(private router: Router, private service: EmpService){
//       this.employee = {
//         empName :"",
//         salary : "",
//         gender: "",
//         doj : "",
//         country : "",
//         emailId : "",
//         password : "",
//         deptId : ""
//       }
//     }
//     registerSubmit(registerForm:any){
//       this.employee.empName = registerForm.empName;
//       this.employee.salary = registerForm.salary;
//       this.employee.gender = registerForm.gender;
//       this.employee.doj = registerForm.doj;
//       this.employee.country = registerForm.country;
//       this.employee.mobilenumber = registerForm.mobilenumber;
//       this.employee.emailId = registerForm.emailId;
//       this.employee.password = registerForm.password;
//       this.employee.deptId = registerForm.deptId
//       console.log(this.employee);
//         alert("Registration Succsses...");
//     }
//   ngOnInit() {
//     this.service.getAllCountries().subscribe((data: any) => {
//       console.log(data);
//       this.countries = data;
//     });
//   }
//   }

export class RegistrationComponent implements OnInit {

  emp: any;
  countries: any;
  departments: any;

  constructor(private router: Router, private service: EmpService) {

    //Making the emp object in such a way that it contains the department json object
    this.emp = {
      empId: '',
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: '',
      department: {
        deptId:''
      }
    }
  }

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
    this.service.getAllDepartments().subscribe((data: any) => { 
      console.log(data);
      this.departments = data; 
    });
  }

  empRegister(regForm: any) {

    //Binding the registerForm data to the emp object, as it contains the department as a Json object
    this.emp.empName = regForm.empName;
    this.emp.salary = regForm.salary;
    this.emp.gender = regForm.gender;
    this.emp.doj = regForm.doj;
    this.emp.country = regForm.country;
    this.emp.emailId = regForm.emailId;
    this.emp.password = regForm.password;
    this.emp.department.deptId = regForm.department;   
    
    console.log(this.emp);
    
    this.service.registerEmplooyee(this.emp).subscribe((data: any) => { console.log(data); });

    alert("Employee Registered Successfully!!!");
    this.router.navigate(['login']);
}
}
